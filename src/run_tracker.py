#!/usr/bin/env python
__author__ = 'Peyton Lee'
__copyright__ = '2020'
__license__ = 'GPL v3'
__contact__ = 'plee at mbari.org'
__doc__ = '''
Configures, runs, and stores output from the deepsea-track docker image.
(Requires Python 3.5 or higher.)

@var __date__: 8/7/2020
@undocumented: __doc__ parser
@status: development
@license: GPL
'''
import argparse
import glob
import json
import os
import shutil
import subprocess
import sys
import time

if sys.version_info[0] < 3:
    raise Exception(
        "run_tracker.py requires a python version >=3.5. Try running with the python3 command?")

# Available OpenCV tracker types, with their names.
TRACKER_TYPES = {
    -1: "NA",
    0: "MEDIANFLOW",
    1: "KCF",
    2: "TLD",
    3: "MOSSE",
    4: "CSRT"
}

def run_docker_tracker(video_src: str, xml_input_dir: str, output_dir: str, tracker1: int, 
                       tracker2: int, resolution: int, stride: int, starting_frame=0, quiet=True):
    """ Runs a docker instance of the deep-sea track module for the given video and 
        XML detection input, using the given configuration.

        Writes JSON output to a subfolder of the output_dir, as follows:
        ```
        {output_dir}
        ├───TRACKER1_TRACKER2_R{resolution}_S{resolution}/
        │   ├───log.txt (docker image output)
        │   ├───config.json (configuration information for this tracker run)
        │   └───json/
        │       ├───f000000.json
        │       ├───f000001.json
        │       ├───f000002.json
        │       └───...
        └───...
        ```

        Params
        ------
        video_src: The path to the input video.
        xml_input_dir: The directory containing the frame-by-frame detections for the input video. 
        output_dir: The directory to write the output folder to. A subfolder for this tracker 
                    configuration will be made, as shown above.
        tracker1: The primary tracker to use.
        resolution:
        stride:
        starting_frame: (optional) The frame number to start at. Set to 0 by default
        quiet: (optional) If false, debug output will be printed (true by default).
    """

    if not os.path.exists(video_src):
        raise FileNotFoundError("No such file '{}'.".format(video_src))

    if not os.path.exists(xml_input_dir) or not os.path.isdir(xml_input_dir):
        raise NotADirectoryError(
            "No such directory '{}'".format(xml_input_dir))

    if not os.path.exists(output_dir):
        if not quiet:
            print("Output directory {} does not exist and will be created.".format(output_dir))
        os.makedirs(output_dir)

    xml_file_paths = glob.glob(xml_input_dir + "/f*.xml")
    num_xml_files = len(xml_file_paths)
    if num_xml_files == 0:
        raise FileNotFoundError(
            "No XML frame files were found in {}.".format(xml_input_dir))

    # Verify that the tracker ID's are valid
    if tracker2 < 0 or tracker1 >= len(TRACKER_TYPES):
        raise RuntimeError("The specified tracker ID {} is invalid. \
                            Valid tracker options are {}.".format(tracker1, TRACKER_TYPES))
    elif tracker2 < 0 or tracker2 >= len(TRACKER_TYPES):
        raise RuntimeError("The specified tracker ID {} is invalid. \
                            Valid tracker options are {}.".format(tracker1, TRACKER_TYPES))

    # Verify that the resolution and stride are valid.
    if starting_frame < 0:
        raise ValueError('Starting frame ID cannot be negative. (Given {})'.format(starting_frame))
    if resolution < 0 or resolution > 1:
        raise ValueError('Resolution must be between 0 and 1. (Given {})'.format(resolution))
    if stride < 1:
        raise ValueError('Stride must be 1 or higher. (Given {})'.format(stride))

    # Verify that there aren't any JSON frames in the xml_input_dir.
    # This may happen if a previous tracker stopped early.
    # Remove if found but print a warning.
    # TODO

    # Print the tracker configurations.
    if not quiet:
        print("\nVideo source: {}".format(video_src))
        print("XML input directory: {} ({} xml frames found.)".format(
            xml_input_dir, len(xml_file_paths)))
        print("Output directory: {}".format(output_dir))
        print("Running trackers {} at resolution {} and stride {}.\n".format(
            list(map(lambda x: (
                TRACKER_TYPES[tracker1] + " / " + TRACKER_TYPES[tracker2]), tracker_configs)),
            resolution,
            stride
        ))

    # Write the deepsea_cfg.json file to the xml_input_dir for the current tracker,
    # to set up the deepsea_track tracker.
    deepsea_cfg = {
        "tracker1": tracker1,
        "tracker2": tracker2,
        "min_event_frames": 0,
        "display_wait_msecs": 0,
        "display": False
    }
    deepsea_cfg_path = os.path.join(xml_input_dir, 'deepsea_cfg.json')
    with open(deepsea_cfg_path, 'wt') as f:
        f.write(json.dumps(deepsea_cfg))
    
    # Create the output directory for the current experiment run.
    # Each experiment has a directory named
    # {tracker1}_{tracker2}_R{resolution}_S{stride}
    # with a /json folder inside, holding all the json framedata.
    exp_dir_name = "{}_{}_R{}_S{}/".format(
        TRACKER_TYPES[tracker1], TRACKER_TYPES[tracker2], resolution, stride)
    exp_dir_path = os.path.join(output_dir, exp_dir_name)
    if not os.path.exists(exp_dir_path):
        os.makedirs(exp_dir_path)
    json_dir = os.path.join(exp_dir_path, 'json/')
    if not os.path.exists(json_dir):
        os.makedirs(json_dir)

    # Generate the docker run command.
    video_src_name = os.path.basename(video_src)
    # Paths need to be absolute or else docker will treat them as mount names instead.
    video_src_dir = os.path.abspath(os.path.dirname(video_src))
    xml_abs_dir = os.path.abspath(xml_input_dir)

    cmd = "docker run --rm -it -v {}/:/mnt/xml/ -v {}/:/mnt/video_src/ pleembari/deepsea-track:1.3 /mnt/video_src/{} /mnt/xml/ {} {} {}"
    cmd = cmd.format(xml_abs_dir, video_src_dir,
                     video_src_name, starting_frame, resolution, stride)
    # Because subprocess.run only accepts args as an array
    cmd_as_args = cmd.split(' ')
    if not quiet:
        print("Running command tracker with command: {}".format(cmd))

    start_time = time.time() # Record the starting time to get duration.

    # Run the tracker command
    process_result = subprocess.run(
        cmd_as_args, stdout=subprocess.PIPE)
    output = process_result.stdout.decode('utf-8')

    if not quiet:
        print("\nTracker Completed.\n")

    end_time = time.time()
    time_elapsed = end_time - start_time

    # Write the output to the log file for the tracker.
    log_path = os.path.join(exp_dir_path, 'log.txt')
    with open(log_path, 'wt') as f:
        f.write(output)

    # Output the run configuration for this directory and the timestamp.
    run_config_data = {
        'tracker1': tracker1,
        'tracker2': tracker2,
        'resolution': resolution,
        'stride': stride,
        'starting_frame': starting_frame,
        'time_elapsed': time_elapsed
    }
    run_config_path = os.path.join(exp_dir_path, 'run_config.json')
    with open(run_config_path, 'wt') as f:
        f.write(json.dumps(run_config_data))

    # Move all the newly-generated JSON files to the experiment output.
    # The tracker places all the JSON output in the same directory as the
    # XML detections, so we have to manually move the files post facto.
    json_file_paths = glob.glob(xml_input_dir + "/f*.json")
    print("Moving {} .json frame files to {}".format(
        len(json_file_paths), json_dir))

    for file_path in json_file_paths:
        filename = os.path.basename(file_path)
        dst_json_path = os.path.join(json_dir, filename)
        shutil.move(file_path, dst_json_path)
    # Done!


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Configures, runs, and organizes output from the deepsea-track tracker.")
    parser.add_argument('video_src', type=str, help="Path to video source.")
    parser.add_argument('xml_input_dir', type=str,
                        help="Path the directory containing the XML input.")
    parser.add_argument('output_dir', type=str, help="Path to the directory for the tracker output. \
                            Subfolders for each tracker run will be created here, with the name \
                            TRACKER1_TRACKER2_R---_S---.")
    parser.add_argument('-t', '--tracker_configs', type=int,
                        default=[0, -1],
                        nargs="+",
                        help="The tracker configurations to use. These should be specified as a list of integers, \
                        where each pair of integers correspond to the first and second trackers to use. e.g. 0 -1 1 3. \
                        The available trackers are {}.".format(TRACKER_TYPES))
    parser.add_argument('-r', '--resolutions', type=float,
                        help="The resolutions (as fractions) to use. Defaults to 0.25 0.5 1.",
                        nargs="+",
                        default=[0.25, 0.5, 1])
    parser.add_argument('-s', '--strides', type=int,
                        help="The strides (in frames) to use. Defaults to 1 10 20.",
                        nargs="+",
                        default=[1, 10, 20])
    args = parser.parse_args()

    video_src = args.video_src
    xml_input_dir = args.xml_input_dir
    output_dir = args.output_dir
    tracker_list = args.tracker_configs
    resolutions = args.resolutions
    strides = args.strides

    tracker_configs = []

    # Verify that the tracker_list is valid.
    if len(tracker_list) % 2 != 0:
        raise RuntimeError("The list of tracker_configs must be an even number.")

    # Run the tracker for each permutation of the run configuration.    
    for i in range(len(tracker_list) / 2):
        tracker1 = tracker_list[2 * i]
        tracker2 = tracker_list[2 * i + 1]
        for stride in strides:
            for resolution in resolutions:
                run_docker_tracker(video_src=video_src, 
                                   xml_input_dir=xml_input_dir, 
                                   output_dir=output_dir, 
                                   tracker1=tracker1, 
                                   tracker2=tracker2, 
                                   starting_frame=0,
                                   resolution=resolution, 
                                   stride=stride,
                                   quiet=False)