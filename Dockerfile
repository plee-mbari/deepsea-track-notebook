FROM mbari/tensorflow:1.13.0rc1-gpu-py3-jupyter
ARG DOCKER_GID
ARG DOCKER_UID
USER root
ADD requirements.txt .
RUN pip3 install -r requirements.txt
# Add non-root user
RUN groupadd --gid $DOCKER_GID plee && adduser --uid $DOCKER_UID --gid $DOCKER_GID --disabled-password --quiet --gecos "" plee_user
