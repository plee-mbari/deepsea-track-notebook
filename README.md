##  Deepsea Tracking Experiment Notebook 

## Requirements
* GPU
* Docker 
* Python 3.6

## Running
1. Build docker image
```
Run mounting this code to retain any changes
./build.sh
```

2. Run jupyter notebook, mounting the local directory as /code in the container

```
docker run --rm -it  -u `id -u`:`id -g` --privileged -e CUDA_VISIBLE_DEVICES=2,3 -p 8889:8888 -v $PWD:/notebooks -t deepsea-track-notebook --allow-root deepsea-track-notebook
```

Should see something similar to the following:
```
bash
[I 06:02:56.449 NotebookApp] Writing notebook server cookie secret to /home/docker_user/.local/share/jupyter/runtime/notebook_cookie_secret
[I 06:02:56.625 NotebookApp] Serving notebooks from local directory: /code
[I 06:02:56.625 NotebookApp] The Jupyter Notebook is running at:
[I 06:02:56.625 NotebookApp] http://d9f97d293e8c:8888/?token=154ce1d1f102f12394cf2cc6ef71f4a49bab76a2b770e62b
[I 06:02:56.625 NotebookApp]  or http://127.0.0.1:8888/?token=154ce1d1f102f12394cf2cc6ef71f4a49bab76a2b770e62b
[I 06:02:56.625 NotebookApp] Use Control-C to stop this server and shut down all kernels (twice to skip confirmation).
[C 06:02:56.629 NotebookApp]

    To access the notebook, open this file in a browser:
        file:///home/docker_user/.local/share/jupyter/runtime/nbserver-1-open.html
    Or copy and paste one of these URLs:
        http://d9f97d293e8c:8888/?token=154ce1d1f102f12394cf2cc6ef71f4a49bab76a2b770e62b
     or http://127.0.0.1:8888/?token=154ce1d1f102f12394cf2cc6ef71f4a49bab76a2b770e62b
```

3. Use the provided URL to access the Jupyter notebook (if running on an EC2 instance, substitute the server address for the IP.)
